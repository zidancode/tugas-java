package tugas;
import java.util.Scanner;

public class Tugas {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nama;
        String nim;
        
        double absen;
        double nilai_tugas;
        double nilai_UTS;
        double nilai_UAS;
        
        double rata;

        System.out.println("PROGRAM NILAI MAHASISWA");
        System.out.println("=======================");
        
        // untuk input nim
        System.out.print("Masukan NIM Anda: ");
        nim = scanner.nextLine();
        
        // untuk input nama
        System.out.print("Masukan Nama Mahasiswa: ");
        nama = scanner.nextLine();
        
        // untuk input absen
        System.out.print("Masukan Nilai Absen: ");
        absen = scanner.nextDouble();
        
        // untuk input nilai tugas
        System.out.print("Masukan Nilai Tugas: ");
        nilai_tugas = scanner.nextDouble();
        
        // untuk input nilai UTS
        System.out.print("Masukan Nilai UTS: ");
        nilai_UTS = scanner.nextDouble();
        
        // untuk input nilai UAS
        System.out.print("Masukan Nilai UAS: ");
        nilai_UAS = scanner.nextDouble();

        // Hasil inputan
        System.out.println("\n===================");
        System.out.println("Nama Anda   : " + nim);
        System.out.println("NIM Anda    : " + nama);
        System.out.println("Nilai Absen : " + absen);
        System.out.println("Nilai Tugas : " + nilai_tugas);
        System.out.println("Nilai UTS   : " + nilai_UTS);
        System.out.println("Nilai UAS   : " + nilai_UTS);
         
        rata = (0.1 * absen) + (0.2 * nilai_tugas) + (0.3 * nilai_UTS) + (0.4 * nilai_UAS);
        System.out.println("=======================");
        System.out.println("Rata-rata   : " + rata);

        scanner.close();
    }
    
}